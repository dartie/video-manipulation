# video-manipolation

Collection of scripts for converting and organizing videos

## Installation

### Linux
```bash
sudo apt install mkvtoolnix mkvtoolnix-gui
```


### Windows
Not needed. `mkvmerge` is shipped as portable executable.


## Usage
```bash
python video2mkv.py rootpath [-m]
```

example

```bash
python video2mkv.py "c:\myvideos" -m
```
