#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Dario Necco"

import os
import sys
import shutil
import threading
import argparse
from argparse import ArgumentParser, RawTextHelpFormatter
from subprocess import Popen, PIPE

# import Colorama
try:
    from colorama import init

    init()

    from colorama import Fore, Back, Style

except ImportError:
    print('Colorama not imported')
# -----------------------------------------------------------


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    video2mkv converts all mp4 videos found in iterating the root directory to mkv, embedding subtitles files if found.

    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=argparse.SUPPRESS)

    parser.add_argument("-m", "--multi-instance", action='store_true',
                        help="If enabled, it runs multiple instances at the same time")

    parser.add_argument("-ext", "--extensions-allowed", default=['mp4', 'avi', 'mkv'], nargs='+',
                        help="Specify the extensions of files to process.")

    parser.add_argument("-suffix", "--oldfile-suffix", dest='suffix_old_mkv', default='',
                        help="Suffix to apply to old mkv files. Include '.mkv' if desired as it's stripped out.")

    parser.add_argument("-k", "--keep-srt", action='store_true', default=False, help=argparse.SUPPRESS)
                        # help="Keeps old srt files")  # not used at moment, hidden

    parser.add_argument("root", help="Iteration root")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


def convert_to_mkv(args, file, cmd):
    # os.system(cmd)
    if args.debug:
        print('{}CMD: {}{}'.format(Fore.BLUE + Style.BRIGHT, cmd, Style.RESET_ALL))
    # cmd = 'SET PATH=c:\portable\mkvtoolnix;%PATH% & ' + cmd
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    p_out, p_err = p.communicate()
    p_rc = p.returncode

    p_out_forecolor = Fore.RED if 'error: ' in p_out.decode('utf-8').lower() else ''

    # '{color}File: "{f}"{reset}\n'.format(color=Fore.MAGENTA, reset=Style.RESET_ALL, f=file) +  # removed at moment
    print(p_out_forecolor + p_out.decode('utf-8') + '{color}'.format(color=Fore.RED) + p_err.decode('utf-8') + '{reset}'.format(reset=Style.RESET_ALL))


def main(args=None):
    if args is None:
        args = check_args()

    if args.root.strip() == '.':
        args.root = os.path.dirname(os.path.realpath(__file__))

    print('{}Looking for files {} from root "{}"{}'.format(Fore.GREEN, args.extensions_allowed, args.root, Style.RESET_ALL))

    suffix_old_mkv = args.suffix_old_mkv  # suffix which is applied only in case a mkv without srt embedded is processed
    missing_srt = []
    thread_list = []
    for root, dirs, files in os.walk(args.root):
        input_files = [x for x in files if os.path.splitext(x)[1].lower().strip('.') in args.extensions_allowed]
        for f in input_files:
            fname = os.path.splitext(f)[0]
            f_fullpath = os.path.realpath(os.path.join(root, f))
            out_mkv = os.path.join(os.path.dirname(f_fullpath), fname + '.mkv')
            expected_srt_fullpath = os.path.join(os.path.dirname(f_fullpath), fname + '.srt')

            if f.lower().endswith('.mkv') and os.path.exists(expected_srt_fullpath):
                old_file = os.path.join(os.path.splitext(f_fullpath)[0] + '{suffix}'.format(suffix=suffix_old_mkv))
                shutil.move(f_fullpath, old_file)
                f_fullpath = old_file

                oldsrt = os.path.join(os.path.splitext(f_fullpath)[0] + '.srt')

                # try:
                #     shutil.copy2(expected_srt_fullpath, oldsrt)
                # except:
                #     pass

            # if the subtitle exists, include it in the video making the mkv
            # if it doesn't and the video file is not a mkv, then just turn the video to mkv.
            # if the file is a mkv and the subtitle doesn't exist, do nothing, as it's useless.
            if os.path.exists(expected_srt_fullpath):
                cmd = 'mkvmerge -o "{out_mkv}" "{input}" --forced-track "0:yes" --default-track "0:yes" --track-name "0:English" --language "0:eng" "{srt}"'.format(
                    out_mkv=out_mkv,
                    input=f_fullpath,
                    srt=expected_srt_fullpath
                )
            else:
                if f.lower().endswith('.mkv'):
                    continue
                else:
                    missing_srt.append(f)

                cmd = 'mkvmerge -o "{out_mkv}" "{input}"'.format(input=f_fullpath, out_mkv=out_mkv)

            print('\n{color}# Processing "{file}" \n# in {filepath}{reset}'.format(file=os.path.basename(f_fullpath), filepath=os.path.dirname(f_fullpath), color=Fore.CYAN + Style.BRIGHT, reset=Style.RESET_ALL))

            if not args.multi_instance:
                convert_to_mkv(args, f_fullpath, cmd)
            else:
                t = threading.Thread(target=convert_to_mkv, args=(args, f_fullpath, cmd))
                thread_list.append(t)
                t.start()

    if args.multi_instance:  # wait all threads are completed
        for t in thread_list:
            t.join()
            pass

    if missing_srt:
        print('The following files are missing the subtitle')
        for missing_srt_f in missing_srt:
            print('{color}{f}{reset}'.format(color=Fore.YELLOW, reset=Style.RESET_ALL, f=missing_srt_f))


if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)